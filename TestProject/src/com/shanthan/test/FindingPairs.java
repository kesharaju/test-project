package com.shanthan.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class FindingPairs {

	public static boolean doesTheArrayContianNPairs(List<Integer> numbers, int numberOfPairs) {
		HashMap<Integer, Integer> resultMap = new HashMap<Integer, Integer>();

		int pairs = 0;
		for (Iterator<Integer> numberIterator = numbers.iterator(); numberIterator.hasNext();) {
			Integer number = numberIterator.next();
			Integer value = resultMap.get(number);

			if (value != null) {
				resultMap.put(number, ++value);
				if (value == 2)
					++pairs;
				else if (value > 2)
					--pairs;
			} else
				resultMap.put(number, 1);

		}
		boolean result = (numberOfPairs == pairs) ? true : false;
		System.out.println("Number of Pairs Found=" + pairs);
		return result;
	}
	
	/* Allow duplicate pairs. For example if the array has four 1s, then count it as 2 pairs */
	
	public static boolean doesTheArrayContianNPairsWithDuplicatesAllowed(List<Integer> numbers, int numberOfPairs) {
		HashMap<Integer, Integer> resultMap = new HashMap<Integer, Integer>();

		int pairs = 0;
		for (Iterator<Integer> numberIterator = numbers.iterator(); numberIterator.hasNext();) {
			Integer number = numberIterator.next();
			Integer value = resultMap.get(number);

			if (value != null) {
				resultMap.put(number, ++value);
			} else
				resultMap.put(number, 1);

		}
		for(Iterator<Integer> iterator = resultMap.keySet().iterator(); iterator.hasNext();)
		{
			int value = resultMap.get(iterator.next());
			if(value%2==0)
			pairs = pairs + (value/2);
		}
		boolean result = (numberOfPairs == pairs) ? true : false;
		System.out.println("Number of Pairs Found=" + pairs);
		return result;
	}

	public static void main(String args[]) {
		Integer noPair[] = { 1, 2, 3, 4, 5 }; /*No Pair*/
		Integer onePair[] = { 1, 1, 2, 3, 4 }; /*One Pair*/
		Integer twoPair[] = { 1, 1, 2, 2, 3 }; /* Two Pair*/
		Integer threeOfAKind[] = { 1, 1, 1, 2, 3 }; /* Three of a Kind */
		Integer fullHouse[] = { 1, 1, 1, 2, 2 }; /* Full House */
		Integer fourOfAKind[] = { 1, 1, 1, 1, 3 }; /* Four of a kind */

		boolean result1 = doesTheArrayContianNPairs(new ArrayList<Integer>(Arrays.asList(noPair)), 2);
		boolean result2 = doesTheArrayContianNPairs(new ArrayList<Integer>(Arrays.asList(onePair)), 2);
		boolean result3 = doesTheArrayContianNPairs(new ArrayList<Integer>(Arrays.asList(twoPair)), 2);
		boolean result4 = doesTheArrayContianNPairs(new ArrayList<Integer>(Arrays.asList(threeOfAKind)), 2);
		boolean result5 = doesTheArrayContianNPairs(new ArrayList<Integer>(Arrays.asList(fullHouse)), 2);
		boolean result6 = doesTheArrayContianNPairs(new ArrayList<Integer>(Arrays.asList(fourOfAKind)), 2);

		System.out.println(result1);
		System.out.println(result2);
		System.out.println(result3);
		System.out.println(result4);
		System.out.println(result5);
		System.out.println(result6);

		/*
		 * Output
		 *  Number of Pairs Found=0 
		 *  Number of Pairs Found=1 
		 *  Number of Pairs Found=2
		 *  Number of Pairs Found=0 
		 *  Number of Pairs Found=1 
		 *  Number of Pair Found=-1 
		 *  false 
		 *  false
		 *  true
		 *  false 
		 *  false 
		 *  false
		 * 
		 */
		
		 result1 = doesTheArrayContianNPairsWithDuplicatesAllowed(new ArrayList<Integer>(Arrays.asList(noPair)), 2);
		 result2 = doesTheArrayContianNPairsWithDuplicatesAllowed(new ArrayList<Integer>(Arrays.asList(onePair)), 2);
		 result3 = doesTheArrayContianNPairsWithDuplicatesAllowed(new ArrayList<Integer>(Arrays.asList(twoPair)), 2);
		 result4 = doesTheArrayContianNPairsWithDuplicatesAllowed(new ArrayList<Integer>(Arrays.asList(threeOfAKind)), 2);
		 result5 = doesTheArrayContianNPairsWithDuplicatesAllowed(new ArrayList<Integer>(Arrays.asList(fullHouse)), 2);
		 result6 = doesTheArrayContianNPairsWithDuplicatesAllowed(new ArrayList<Integer>(Arrays.asList(fourOfAKind)), 2);

		System.out.println(result1);
		System.out.println(result2);
		System.out.println(result3);
		System.out.println(result4);
		System.out.println(result5);
		System.out.println(result6);

	   // Four of a kind will be counted as 2 Pairs, since duplicate pairs are allowed. However, full house wont be counted as two pairs since it is a three of a kind and a pair.
		
		
	  /*Number of Pairs Found=0
		Number of Pairs Found=1
		Number of Pairs Found=2
		Number of Pairs Found=0
		Number of Pairs Found=1
		Number of Pairs Found=2
		false
		false
		true
		false
		false
		true*/

	}
}